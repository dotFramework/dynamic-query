using DotFramework.Core;
using DotFramework.DynamicQuery.Metadata;
using System.Linq.Expressions;

namespace DotFramework.DynamicQuery
{
    public class OrderByFactory : SingletonProvider<OrderByFactory>
    {
        public OrderBy OrderBy(LambdaExpression expression, OrderByType orderByType)
        {
            var propInfo = expression.GetPropertyInfo();
            string tableName = MetadataHelper.GetGeneralObject(propInfo.MemberType).TableName;
            string fieldName = propInfo.Property.Name;

            OrderBy orderBy = new OrderBy
            {
                TableName = tableName,
                FieldName = fieldName,
                OrderByType = orderByType
            };

            return orderBy;
        }
    }
}
