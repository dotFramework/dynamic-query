using System;

namespace DotFramework.DynamicQuery.Metadata
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SchemaMetadataAttribute : Attribute
    {
        public string SchemaName { get; set; }
        public string TableName { get; set; }
        public string TablePrefix { get; set; }

        public string GetTableName(string typeName)
        {
            if (!String.IsNullOrWhiteSpace(TableName))
            {
                return String.IsNullOrWhiteSpace(TablePrefix) ? TableName : TablePrefix + TableName;
            }
            else
            {
                return String.IsNullOrWhiteSpace(TablePrefix) ? typeName : TablePrefix + typeName;
            }
        }
    }
}
